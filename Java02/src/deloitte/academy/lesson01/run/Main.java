package deloitte.academy.lesson01.run;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import deloitte.academy.lesson.array.Booleanos;
import deloitte.academy.lesson.array.Cursos;
import deloitte.academy.lesson.array.Entero;
import deloitte.academy.lesson.array.Strings;

/**
 * Clase donde se encuentra el main y se ejecutaran todas las funciones
 * realizadas
 * 
 * @author mvillegas
 *
 */
public class Main {
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int opcion = 0;
		int[] pruebas = { 2, 5, 9, 4, 2, 4 };
		List<Integer> myCoords = new ArrayList<Integer>();
		myCoords.add(1);
		myCoords.add(2);
		myCoords.add(3);
		boolean[] pruebasBool = { true, true, false, false, true };

		while (opcion != 9) {

			System.out.println("Elige las funciones que desee revisar");
			System.out.println("1.-Enteros");
			System.out.println("2.-Booleanos");
			System.out.println("3.-Strings");
			System.out.println("4.-Cursos");
			System.out.println("9.-Salir");
			opcion = Integer.parseInt(br.readLine());
			switch (opcion) {

			case 1:
				System.out.println("Arreglo original: ");
				Entero.imprimeArreglo(pruebas);
				System.out.println("Numero mayor: " + Entero.numeroMayor(pruebas));
				System.out.println("Numero menor: " + Entero.numeroMenor(pruebas));
				Entero.imprimeArreglo(Entero.repetidos(pruebas));
				System.out.println("\n");
				Entero.impimeLista(myCoords);

				break;
			case 2:
				for (int i = 0; i < pruebasBool.length; i++) {
					System.out.print(pruebasBool[i] + " ");
				}
				System.out.println("\n");
				Booleanos.imprimeBooleanos(Booleanos.separar(pruebasBool));
				break;
			case 3:
				System.out.println("Frase: Hola soy mario Palabra: Hola");
				System.out.println(Strings.encontrarPalabra("Hola soy mario", "Hola"));
				break;
			case 4:
				Cursos.agregaCurso("02-04-12", "Qu�mica");
				Cursos.agregaCurso("01-01-00", "Ciencias");
				Cursos.agregaCurso("02-04-12", "Filosofia");
				System.out.println("Cursos sin ordenar");
				Cursos.imprimeCurso();
				Cursos.ordenarCurso();
				System.out.println("Cursos ordenados");
				Cursos.imprimeCurso();
				System.out.println("\n");

				break;
			default:
				System.out.println("Opci�n no v�lida");
				break;

			}
		}

	}

}
