package deloitte.academy.lesson.array;

/**
 * Clase con todas las funciones relacionadas con strings
 * 
 * @author mvillegas
 *
 */
public class Strings {
	/**
	 * Regresa un true si la palabra deseada esta dentro de la frase
	 * 
	 * @param frase   String
	 * @param palabra String
	 * @return
	 */
	public static boolean encontrarPalabra(String frase, String palabra) {
		boolean result = false;
		int longitud = palabra.length();
		for (int i = 0; i < frase.length() - longitud; i++) {
			// System.out.println(frase.substring(i,i+longitud)+" = "+palabra);
			if (frase.contains(palabra)) {
				result = true;

			}

		}
		return result;
	}
}
