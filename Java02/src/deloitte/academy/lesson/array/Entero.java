package deloitte.academy.lesson.array;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase con todas las funcionalidades que tengan relacion con enteros
 * 
 * @author mvillegas
 *
 */
public class Entero {
	private static final Logger LOGGER = Logger.getLogger(Entero.class.getName());

	int[] arregloEnteros;

	/**
	 * Constructor b�sico
	 */
	public Entero() {

	}

	/**
	 * Constructor con par�metros
	 * 
	 * @param arregloEnteros
	 */
	public Entero(int[] arregloEnteros) {
		super();
		this.arregloEnteros = arregloEnteros;
	}

	/**
	 * numeroMayor recibe un arreglo de enteros y regresa un entero como resultado
	 * 
	 * @param arreglo int []
	 * @return int
	 */
	public static int numeroMayor(int[] arreglo) {
		int max = 0;
		try {
			for (int num : arreglo) {
				if (num > max)
					max = num;
			}
			LOGGER.info("Funcion numeroMayor realizada correctamente");

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e.getMessage());

		}
		return max;
	}

	/**
	 * numeroMenor recibe un arreglo de enteros y regresa un entero como resultado
	 * 
	 * @param arreglo int []
	 * @return int
	 */
	public static int numeroMenor(int[] arreglo) {
		int min = arreglo[0];
		try {
			for (int num : arreglo) {
				if (num < min)
					min = num;
			}
			LOGGER.info("Funcion numeroMayor realizada correctamente");

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e.getMessage());

		}
		return min;
	}

	/**
	 * Funcion que recibe un arreglo de enteros y regresa un arreglo con los numeros
	 * repetidos de este
	 * 
	 * @param arreglo int[]
	 * @return int[]
	 */
	public static int[] repetidos(int[] arreglo) {
		int[] repetidos = new int[10];
		int numero;
		int repetido;
		int contador = 0;
		try {
			for (int i = 0; i < arreglo.length; i++) {
				for (int j = i + 1; j < arreglo.length; j++) {
					if (arreglo[i] == arreglo[j]) {
						repetidos[contador] = arreglo[j];
						contador++;
					}
				}
			}
			LOGGER.info("Funcion repetidosr realizada correctamente");

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e.getMessage());

		}
		return repetidos;
	}

	/**
	 * Esta funcion recibe un arreglo numerico y lo imprime en pantalla
	 * 
	 * @return
	 */
	public static void imprimeArreglo(int[] arreglo) {
		for (int i = 0; i < arreglo.length; i++) {
			if (arreglo[i] != 0)
				System.out.print(arreglo[i] + "  ");
		}
		System.out.println("\n");
	}

	public int[] getArregloEnteros() {
		return arregloEnteros;
	}

	public void setArregloEnteros(int[] arregloEnteros) {
		this.arregloEnteros = arregloEnteros;
	}

	public static void impimeLista(List<Integer> a) 
	{
		for(Integer num : a)
			System.out.println(num);
	}
}
