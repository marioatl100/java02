package deloitte.academy.lesson.array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Clase curso con todas las funciones relacionadas a cursos
 * 
 * @author mvillegas
 *
 */
public class Cursos implements Comparable<Cursos>{
	String fecha;
	String nombre;
	private static final Logger LOGGER = Logger.getLogger(Booleanos.class.getName());

	public static List<Cursos> ListaOficial = new ArrayList<Cursos>();

	public Cursos(String string, String nombre) {
		super();
		this.fecha = string;
		this.nombre = nombre;
	}


	public Cursos() {

	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Con esta funcion seras capaz de agregar un curso
	 * 
	 * @param fecha
	 * @param nombre
	 * @return
	 */
	public static void agregaCurso(String fecha, String nombre) {
		try {
			Cursos aux = new Cursos(fecha, nombre);
			LOGGER.info("Funcion agregaCurso realizada correctamente");

			ListaOficial.add(aux);

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e.getMessage());

		}

	}

	/**
	 * Funcion que recibe el index para eliminar un elemento de la lista
	 * 
	 * @param index
	 */
	public static void borraCurso(int index) {
		try {
			ListaOficial.remove(index);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e.getMessage());

		}

	}




	@Override
	public int compareTo(Cursos curso) {
		 if (getNombre() == null || curso.getNombre() == null) {
		      return 0;
		    }
		    return getNombre().compareTo(curso.getNombre());
	}
	
	/**
	 * Ordena los cursos
	 */
	public static void ordenarCurso() 
	{
		Collections.sort(ListaOficial);
		
	}


	@Override
	public String toString() {
		return "Cursos "
				+ "Fecha=" + fecha + ", Nombre=" + nombre ;
	}
	/**
	 * Imprime los cursos
	 */
	public static void imprimeCurso() 
	{
		for (Cursos a: ListaOficial) 
		{
			System.out.println(a.toString());
		}
	}
	

}
