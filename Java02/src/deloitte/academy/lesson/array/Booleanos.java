package deloitte.academy.lesson.array;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Clase con los metodos relacionados con booleanos
 * @author mvillegas
 *
 */
public class Booleanos {
	private static final Logger LOGGER = Logger.getLogger(Booleanos.class.getName());

	
	boolean list[] = new boolean[10];
	/**
	 * Recibe un arreglo de booleanos y regresa una lista con los dos
	 *  arreglos de booleanos separados
	 * @param lista
	 * @return
	 */
	public static List<Integer> separar(boolean[] lista)
	{
		List<Integer> separacion = new ArrayList<Integer>();
		Booleanos Trues= new Booleanos();
		int contadort=0;
		int contadorf=0;
		Booleanos Falses=new Booleanos();
		
		LOGGER.info("Funcion separar realizada correctamente");

		for(boolean aux: lista) {
			if(aux == true) 
			{
				Trues.list[contadort]=true;
				contadort++;
			}else 
			{
				Falses.list[contadorf]=false;
				contadorf++;
			}
			
			
		}
		
		separacion.add(contadort);
		separacion.add(contadorf);
		return separacion;
	}
	/**
	 * Recibe una lista de booleanos y los imprime
	 * @param lista
	 */
	public static void imprimeBooleanos(List<Integer> lista) 
	{
		LOGGER.info("Funcion imprimeBooleanos realizada correctamente");

		int i=0;
		for (Integer aux: lista) 
		{
			
			if(i==0) {
				System.out.print("True: [");
			for(int j=0;j<aux.intValue();j++) 
			
				System.out.print("true ");
			System.out.println("]");
				i++;
			}
			else 
			{
				System.out.print("False: [");
				for(int k=0;k<aux.intValue();k++) 
				System.out.print("false ");
				System.out.println("]");

			}
			
		}
	}
	
	
	public boolean[] getList() {
		return list;
	}

	public void setList(boolean[] list) {
		this.list = list;
	}

	
}
